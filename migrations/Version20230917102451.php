<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230917102451 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE gift_list (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gift_list_user (gift_list_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_5E12A4CA51F42524 (gift_list_id), INDEX IDX_5E12A4CAA76ED395 (user_id), PRIMARY KEY(gift_list_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gift_list_family (gift_list_id INT NOT NULL, family_id INT NOT NULL, INDEX IDX_B43294251F42524 (gift_list_id), INDEX IDX_B432942C35E566A (family_id), PRIMARY KEY(gift_list_id, family_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE gift_list_user ADD CONSTRAINT FK_5E12A4CA51F42524 FOREIGN KEY (gift_list_id) REFERENCES gift_list (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE gift_list_user ADD CONSTRAINT FK_5E12A4CAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE gift_list_family ADD CONSTRAINT FK_B43294251F42524 FOREIGN KEY (gift_list_id) REFERENCES gift_list (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE gift_list_family ADD CONSTRAINT FK_B432942C35E566A FOREIGN KEY (family_id) REFERENCES family (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gift_list_user DROP FOREIGN KEY FK_5E12A4CA51F42524');
        $this->addSql('ALTER TABLE gift_list_user DROP FOREIGN KEY FK_5E12A4CAA76ED395');
        $this->addSql('ALTER TABLE gift_list_family DROP FOREIGN KEY FK_B43294251F42524');
        $this->addSql('ALTER TABLE gift_list_family DROP FOREIGN KEY FK_B432942C35E566A');
        $this->addSql('DROP TABLE gift_list');
        $this->addSql('DROP TABLE gift_list_user');
        $this->addSql('DROP TABLE gift_list_family');
    }
}
