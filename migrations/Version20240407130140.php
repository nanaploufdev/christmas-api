<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240407130140 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE family DROP FOREIGN KEY FK_A5E6215B7E3C61F9');
        $this->addSql('ALTER TABLE gift DROP FOREIGN KEY FK_A47C990D6C755722');
        $this->addSql('ALTER TABLE family ADD CONSTRAINT FK_A5E6215B7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE gift ADD CONSTRAINT FK_A47C990D6C755722 FOREIGN KEY (buyer_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user ADD email VARCHAR(180) NOT NULL, ADD password VARCHAR(255) NOT NULL, ADD roles JSON NOT NULL');
        $this->addSql('ALTER TABLE kid DROP FOREIGN KEY FK_4523887C9B6B5FBA');
        $this->addSql('ALTER TABLE kid DROP FOREIGN KEY FK_4523887CA76ED395');
        $this->addSql('ALTER TABLE account DROP FOREIGN KEY FK_7D3656A4A76ED395');
        $this->addSql('ALTER TABLE family DROP FOREIGN KEY FK_A5E6215B7E3C61F9');
        $this->addSql('ALTER TABLE gift DROP FOREIGN KEY FK_A47C990D6C755722');
        $this->addSql('DROP TABLE kid');
        $this->addSql('UPDATE user u JOIN account a ON u.id = a.id SET u.email = a.email, u.password = a.password, u.roles = a.roles;');
        $this->addSql('DELETE FROM gift WHERE gift_list_id IN (SELECT id FROM gift_list WHERE user_id IN (SELECT id FROM user WHERE email IS NULL OR email = \'\'))');
        $this->addSql('DELETE FROM gift_list WHERE user_id IN (SELECT id FROM user WHERE email IS NULL OR email = \'\')');
        $this->addSql('DELETE FROM family WHERE owner_id IN (SELECT id FROM user WHERE email IS NULL OR email = \'\')');
        $this->addSql('DELETE FROM user WHERE email IS NULL OR email = \'\'');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON user (email)');
        $this->addSql('DROP TABLE account');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE kid (id INT AUTO_INCREMENT NOT NULL, account_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_4523887C9B6B5FBA (account_id), UNIQUE INDEX UNIQ_4523887CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE account (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, email VARCHAR(180) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, password VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, roles JSON NOT NULL, UNIQUE INDEX UNIQ_7D3656A4A76ED395 (user_id), UNIQUE INDEX UNIQ_7D3656A4E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE kid ADD CONSTRAINT FK_4523887C9B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE kid ADD CONSTRAINT FK_4523887CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE account ADD CONSTRAINT FK_7D3656A4A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE gift DROP FOREIGN KEY FK_A47C990D6C755722');
        $this->addSql('ALTER TABLE gift ADD CONSTRAINT FK_A47C990D6C755722 FOREIGN KEY (buyer_id) REFERENCES account (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE family DROP FOREIGN KEY FK_A5E6215B7E3C61F9');
        $this->addSql('ALTER TABLE family ADD CONSTRAINT FK_A5E6215B7E3C61F9 FOREIGN KEY (owner_id) REFERENCES account (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('DROP INDEX UNIQ_8D93D649E7927C74 ON user');
        $this->addSql('ALTER TABLE user DROP email, DROP password, DROP roles');
    }
}
