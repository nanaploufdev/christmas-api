<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230917102938 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gift_list_user DROP FOREIGN KEY FK_5E12A4CA51F42524');
        $this->addSql('ALTER TABLE gift_list_user DROP FOREIGN KEY FK_5E12A4CAA76ED395');
        $this->addSql('DROP TABLE gift_list_user');
        $this->addSql('ALTER TABLE gift_list ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE gift_list ADD CONSTRAINT FK_B6B50A45A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_B6B50A45A76ED395 ON gift_list (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE gift_list_user (gift_list_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_5E12A4CAA76ED395 (user_id), INDEX IDX_5E12A4CA51F42524 (gift_list_id), PRIMARY KEY(gift_list_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE gift_list_user ADD CONSTRAINT FK_5E12A4CA51F42524 FOREIGN KEY (gift_list_id) REFERENCES gift_list (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE gift_list_user ADD CONSTRAINT FK_5E12A4CAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE gift_list DROP FOREIGN KEY FK_B6B50A45A76ED395');
        $this->addSql('DROP INDEX IDX_B6B50A45A76ED395 ON gift_list');
        $this->addSql('ALTER TABLE gift_list DROP user_id');
    }
}
