<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230916185942 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE kid (id INT AUTO_INCREMENT NOT NULL, account_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_4523887C9B6B5FBA (account_id), UNIQUE INDEX UNIQ_4523887CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE kid ADD CONSTRAINT FK_4523887C9B6B5FBA FOREIGN KEY (account_id) REFERENCES `account` (id)');
        $this->addSql('ALTER TABLE kid ADD CONSTRAINT FK_4523887CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE account CHANGE user_id user_id INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE kid DROP FOREIGN KEY FK_4523887C9B6B5FBA');
        $this->addSql('ALTER TABLE kid DROP FOREIGN KEY FK_4523887CA76ED395');
        $this->addSql('DROP TABLE kid');
        $this->addSql('ALTER TABLE `account` CHANGE user_id user_id INT DEFAULT NULL');
    }
}
