<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230917093901 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE family (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, name VARCHAR(255) NOT NULL, token VARCHAR(255) NOT NULL, INDEX IDX_A5E6215B7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE family_user (family_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_F9E4746DC35E566A (family_id), INDEX IDX_F9E4746DA76ED395 (user_id), PRIMARY KEY(family_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE family ADD CONSTRAINT FK_A5E6215B7E3C61F9 FOREIGN KEY (owner_id) REFERENCES `account` (id)');
        $this->addSql('ALTER TABLE family_user ADD CONSTRAINT FK_F9E4746DC35E566A FOREIGN KEY (family_id) REFERENCES family (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE family_user ADD CONSTRAINT FK_F9E4746DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE family DROP FOREIGN KEY FK_A5E6215B7E3C61F9');
        $this->addSql('ALTER TABLE family_user DROP FOREIGN KEY FK_F9E4746DC35E566A');
        $this->addSql('ALTER TABLE family_user DROP FOREIGN KEY FK_F9E4746DA76ED395');
        $this->addSql('DROP TABLE family');
        $this->addSql('DROP TABLE family_user');
    }
}
