# Christman API

You can find the project name in the `composer.json` file.

## Installation

1. Clone the repository:

```shell
git clone git@gitlab.com:broken.fr/atelierdenoel/christmas-api.git
```

2. Navigate to the project directory:

```shell
cd christmas-api
```

3. Install the dependencies using Composer:

```shell
composer install
```

4. Copy the `.env.example` file and rename it to `.env`:

```shell
cp .env.example .env
```

5. Update the `.env` file with your database credentials.

6. Run the migrations:

```shell
php bin/console doctrine:migrations:migrate
```

7. Generate jwt key pair:

```shell 
php bin/console lexik:jwt:generate-keypair 
```


## Starting the Project with Docker Compose

To start the project with Docker Compose, run:

```shell
docker-compose up -d
```

## Generate Swagger UI JSON

To generate the `swagger-ui.json` file, you can use the `api:swagger:export` command provided by API Platform:

```shell
php bin/console api:swagger:export > swagger-ui.json
```

This will generate a `swagger-ui.json` file in the root directory of your project.

## Accessing the Project

Once the Docker containers are up and running, you can access the project by navigating to `http://localhost:8000` in
your web browser.
