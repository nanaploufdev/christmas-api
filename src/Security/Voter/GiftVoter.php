<?php

namespace App\Security\Voter;

use App\Entity\Family;
use App\Entity\Gift;
use App\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Serializer\SerializerInterface;

class GiftVoter extends Voter
{
    public const VIEW = 'GIFT_VIEW';
    public const EDIT = 'GIFT_EDIT';
    public const CREATE = 'GIFT_CREATE';
    public const DELETE = 'GIFT_DELETE';

    public function __construct(private readonly RequestStack $requestStack, private SerializerInterface $serializer)
    {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        return in_array($attribute, [self::CREATE, self::DELETE, self::EDIT, self::VIEW])
            && $subject instanceof Gift;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        assert($user instanceof User);
        switch ($attribute) {
            case self::EDIT:
                $conditions = [
                    $this->isUserOfGiftList($user, $subject) && !$subject->isSurprised(),
                    !$this->isUserOfGiftList($user, $subject) && $subject->isSurprised(),
                ];

                $granted = false;
                foreach ($conditions as $condition) {
                    if ($condition) {
                        $granted = true;
                    }
                }
                if (!$granted) {
                    $request = $this->requestStack->getCurrentRequest();
                    $jsonContent = $request->getContent();
                    $patchParams = $this->serializer->decode($jsonContent, 'json');
                    $allowedKeys = ['buyer', 'selected', 'purchased'];
                    return count(array_diff(array_keys($patchParams), $allowedKeys)) === 0;
                }
                return true;
                break;
            case self::VIEW:
                $conditions = [
                    $this->hasOneFamilyInCommonWithGiftList($user, $subject),
                    $this->isUserOwnerAndGiftSurprise($user, $subject)
                ];
                foreach ($conditions as $condition) {
                    if (!$condition) {
                        return false;
                    }
                }
                return true;
        }

        return true;
    }

    protected function isUserOfGiftList(User $user, Gift $subject): bool
    {
        if ($subject->getGiftList()->getUser() === $user) {
            return true;
        }
        return false;
    }

    protected function hasOneFamilyInCommonWithGiftList(User $user, Gift $subject): bool
    {
        $userFamilies = $user->getFamilies();
        $commonValues = $subject->getGiftList()->getFamilies()->filter(function (Family $giftFamily) use ($userFamilies) {
            return $userFamilies->contains($giftFamily);
        });
        if ($commonValues->isEmpty() && $subject->getGiftList()->getUser() !== $user) {
            return false;
        }

        return true;
    }

    protected function isUserOwnerAndGiftSurprise(User $user, Gift $subject): bool
    {
        return !($subject->isSurprised() && $subject->getGiftList()->getUser() === $user);
    }
}
