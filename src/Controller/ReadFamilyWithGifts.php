<?php

namespace App\Controller;

use App\Entity\Family;
use App\Repository\FamilyRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

#[AsController]
class ReadFamilyWithGifts
{
    public function __construct(private readonly FamilyRepository $familyRepository)
    {
    }

    public function __invoke(Request $request): ?Family
    {
        $token = $request->attributes->get('token');
        $family = $this->familyRepository->findFamilyWithoutUsersData($token);
        if (!$family) {
            throw new NotFoundHttpException(sprintf("There is no family with token %s", $token));
        }
        return $family;
    }
}