<?php

namespace App\Controller;

use ApiPlatform\Metadata\IriConverterInterface;
use App\Entity\Family;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

#[AsController]
final class AddUserToFamily
{

    public function __construct(
        private EntityManagerInterface $entityManager,
        private IriConverterInterface $iriConverter
    ) {
    }

    public function __invoke($data, Family $family, Request $request): Family
    {
        try {
            $user = $this->iriConverter->getResourceFromIri($data->id);
        } catch (\Exception $e) {
            throw new NotFoundHttpException(sprintf("There is no user with id %s", $data->id));
        }

        if (null === $user) {
            throw new NotFoundHttpException(sprintf("There is no user with id %s", $data->id));
        }
        if ($user instanceof User === false) {
            throw new InvalidArgumentException(sprintf("%s is not a valid instance of %s", $data->id, User::class));
        }

        $family->addUser($user);
        $this->entityManager->persist($family);
        $this->entityManager->flush();

        return $family;
    }
}