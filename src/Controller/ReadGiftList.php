<?php

namespace App\Controller;

use App\Entity\GiftList;
use App\Repository\GiftListRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

#[AsController]
class ReadGiftList
{
    public function __construct(private readonly GiftListRepository $giftListRepository)
    {
    }

    public function __invoke(Request $request): ?GiftList
    {
        $id = $request->attributes->get('id');
        $giftList = $this->giftListRepository->findGiftListWithoutUsersData($id);
        if (!$giftList) {
            throw new NotFoundHttpException(sprintf("There is no GiftList with token %s", $id));
        }
        return $giftList;
    }
}