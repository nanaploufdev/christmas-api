<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Controller\AddUserToFamily;
use App\Controller\ReadFamilyWithGifts;
use App\Dto\UserDto;
use App\Repository\FamilyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: FamilyRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(),
        new Post(),
        new Get(),
        new Delete(),
        new Patch(),
        new Patch(
            uriTemplate: '/families/{token}/user',
            controller: AddUserToFamily::class,
            input: UserDto::class,
            output: Family::class,
            read: false,
            name: 'add_user',
            denormalizationContext: ['groups' => ['family:add_user']]
        ),
        new Get(
            uriTemplate: '/families/{token}/withGifts',
            controller: ReadFamilyWithGifts::class,
            normalizationContext: ['groups' => ['family:read', 'user:read', 'gift:read'], 'enable_max_depth' => true],
            output: Family::class,
            read: false,
            name: 'read_family_with_gifts'
        )
    ],
    normalizationContext: ['groups' => ['family:read'], 'enable_max_depth' => true],
    denormalizationContext: ['groups' => ['family:create', 'family:update']],
)]
class Family
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(identifier: false)]
    #[Groups(['family:read'])]
    #[MaxDepth(1)]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['family:create', 'family:update', 'family:read', 'user:read'])]
    #[MaxDepth(1)]
    private ?string $name = null;

    #[ORM\Column(length: 255, unique: true)]
    #[ApiProperty(identifier: true)]
    #[Groups(['family:read', 'family:update'])]
    #[MaxDepth(1)]
    private ?string $token;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'families')]
    #[MaxDepth(1)]
    #[Link(toProperty: 'families')]
    #[Groups(['family:create', 'family:update', 'family:read', 'family:add_user'])]
    private Collection $users;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['family:create', 'family:update', 'family:read'])]
    #[MaxDepth(1)]
    private ?User $owner = null;

    #[ORM\ManyToMany(targetEntity: GiftList::class, mappedBy: 'families')]
    #[MaxDepth(1)]
    #[Groups(['family:create', 'family:update'])]
    private Collection $giftLists;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->token = Uuid::v7()->toBase58();
        $this->giftLists = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): static
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @param Collection<int, User> $users
     * @return $this
     */
    public function setUsers(Collection $users): static
    {
        $this->users = $users;

        return $this;
    }

    public function addUser(User $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
        }

        return $this;
    }

    public function removeUser(User $user): static
    {
        $this->users->removeElement($user);

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): static
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection<int, GiftList>
     */
    public function getGiftLists(): Collection
    {
        return $this->giftLists;
    }

    public function addGiftList(GiftList $giftList): static
    {
        if (!$this->giftLists->contains($giftList)) {
            $this->giftLists->add($giftList);
            $giftList->addFamily($this);
        }

        return $this;
    }

    public function removeGiftList(GiftList $giftList): static
    {
        if ($this->giftLists->removeElement($giftList)) {
            $giftList->removeFamily($this);
        }

        return $this;
    }
}
