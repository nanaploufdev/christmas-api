<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Controller\ReadGiftList;
use App\Repository\GiftListRepository;
use App\Traits\TimestampableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

#[ORM\Entity(repositoryClass: GiftListRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(),
        new Post(securityPostDenormalize: "(user == object.getUser())"),
        new Get(
            controller: ReadGiftList::class,
            normalizationContext: ['groups' => ['gift:read', 'gift_list:read'], 'enable_max_depth' => true],
            output: GiftList::class,
            read: false,
            name: 'read_gift_list'
        ),
        new Patch(securityPostDenormalize: "(user == object.getUser())"),
        new Delete(securityPostDenormalize: "(user == object.getUser())"),
    ],
    normalizationContext: ['groups' => ['gift_list:read', 'timestampable'], 'enable_max_depth' => true],
    denormalizationContext: ['groups' => ['gift_list:create', 'gift_list:update']],
)]
class GiftList
{
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['gift_list:read', 'user:read'])]
    #[MaxDepth(1)]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['gift_list:create', 'gift_list:update', 'gift_list:read', 'user:read', 'gift:read'])]
    #[MaxDepth(1)]
    private ?string $name = null;

    #[ORM\Column]
    #[Groups(['gift_list:create', 'gift_list:update', 'gift_list:read', 'user:read', 'gift:read'])]
    #[MaxDepth(1)]
    private bool $kid = false;

    #[ORM\Column(length: 255)]
    #[Groups(['gift_list:create', 'gift_list:update', 'gift_list:read', 'user:read', 'gift:read'])]
    #[MaxDepth(1)]
    private ?string $kidName = null;

    #[ORM\ManyToMany(targetEntity: Family::class, inversedBy: 'giftLists')]
    #[Groups(['gift_list:read', 'gift_list:create', 'gift_list:update', 'user:read'])]
    #[MaxDepth(1)]
    private Collection $families;

    #[ORM\ManyToOne(inversedBy: 'giftLists')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['gift_list:create', 'gift_list:update', 'gift_list:read', 'gift:read'])]
    #[MaxDepth(1)]
    private ?User $user = null;

    #[ORM\OneToMany(mappedBy: 'giftList', targetEntity: Gift::class, orphanRemoval: true)]
    #[Groups(['gift_list:read', 'user:read'])]
    #[MaxDepth(1)]
    private Collection $gifts;

    public function __construct()
    {
        $this->families = new ArrayCollection();
        $this->gifts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function isKid(): bool
    {
        return $this->kid;
    }

    public function setKid(bool $kid): GiftList
    {
        $this->kid = $kid;
        return $this;
    }

    public function getKidName(): ?string
    {
        return $this->kidName;
    }

    public function setKidName(?string $kidName): GiftList
    {
        $this->kidName = $kidName;
        return $this;
    }

    /**
     * @return Collection<int, Family>
     */
    public function getFamilies(): Collection
    {
        return $this->families;
    }

    public function addFamily(Family $family): static
    {
        if (!$this->families->contains($family)) {
            $this->families->add($family);
        }

        return $this;
    }

    public function removeFamily(Family $family): static
    {
        $this->families->removeElement($family);

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, Gift>
     */
    public function getGifts(): Collection
    {
        return $this->gifts;
    }

    public function addGift(Gift $gift): static
    {
        if (!$this->gifts->contains($gift)) {
            $this->gifts->add($gift);
            $gift->setGiftList($this);
        }

        return $this;
    }

    public function removeGift(Gift $gift): static
    {
        if ($this->gifts->removeElement($gift)) {
            // set the owning side to null (unless already changed)
            if ($gift->getGiftList() === $this) {
                $gift->setGiftList(null);
            }
        }

        return $this;
    }
}
