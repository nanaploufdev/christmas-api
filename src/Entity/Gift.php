<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Repository\GiftRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: GiftRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
//            security: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\GiftVoter::VIEW\'), object)'
        ),
        new Post(securityPostDenormalize: "(user == object.getGiftList().getUser() && !object.isSurprised()) || (object.isSurprised() && user != object.getGiftList().getUser())"),
        new Post(securityPostDenormalize: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\GiftVoter::EDIT\'), object)'),
        new Get(security: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\GiftVoter::VIEW\'), object)'),
        new Patch(securityPostDenormalize: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\GiftVoter::EDIT\'), object)'),
        new Delete(securityPostDenormalize: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\GiftVoter::EDIT\'), object)'),
    ],
    normalizationContext: ['groups' => ['gift:read'], 'enable_max_depth' => true],
    denormalizationContext: ['groups' => ['gift:create', 'gift:update']],
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => 'exact', 'buyer' => 'exact'])]
class Gift
{
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['gift_list:read', 'gift:read', 'gift:create', 'gift:update'])]
    #[MaxDepth(1)]
    private ?int $id = null;

    #[Assert\NotBlank]
    #[ORM\Column(length: 255)]
    #[Groups(['gift_list:read', 'gift:read', 'gift:create', 'gift:update'])]
    #[MaxDepth(1)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['gift_list:read', 'gift:read', 'gift:create', 'gift:update'])]
    #[MaxDepth(1)]
    private ?string $description = null;

    #[ORM\Column(length: 1020, nullable: true)]
    #[Groups(['gift_list:read', 'gift:read', 'gift:create', 'gift:update'])]
    #[MaxDepth(1)]
    private ?string $link = null;

    #[ORM\Column(length: 1020, nullable: true)]
    #[Groups(['gift_list:read', 'gift:read', 'gift:create', 'gift:update'])]
    #[MaxDepth(1)]
    private ?string $linkBis = null;

    #[ORM\Column]
    #[Groups(['gift_list:read', 'gift:read', 'gift:create', 'gift:update'])]
    #[MaxDepth(1)]
    private ?bool $selected = false;

    #[ORM\ManyToOne(inversedBy: 'buyingGifts')]
    #[Groups(['gift_list:read', 'gift:read', 'gift:create', 'gift:update'])]
    #[MaxDepth(1)]
    private ?User $buyer = null;

    #[ORM\Column]
    #[Groups(['gift_list:read', 'gift:read', 'gift:create', 'gift:update'])]
    #[MaxDepth(1)]
    private ?bool $purchased = false;

    #[ORM\Column]
    #[Groups(['gift_list:read', 'gift:read', 'gift:create', 'gift:update'])]
    #[MaxDepth(1)]
    private ?bool $surprised = false;

    #[Assert\NotBlank]
    #[ORM\ManyToOne(inversedBy: 'gifts')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['gift_list:read', 'gift:read', 'gift:create', 'gift:update'])]
    #[MaxDepth(1)]
    private ?GiftList $giftList = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): static
    {
        $this->link = $link;

        return $this;
    }

    public function getLinkBis(): ?string
    {
        return $this->linkBis;
    }

    public function setLinkBis(?string $linkBis): static
    {
        $this->linkBis = $linkBis;

        return $this;
    }

    public function isSelected(): ?bool
    {
        return $this->selected;
    }

    public function setSelected(bool $selected): static
    {
        $this->selected = $selected;

        return $this;
    }

    public function getBuyer(): ?User
    {
        return $this->buyer;
    }

    public function setBuyer(?User $buyer): static
    {
        $this->buyer = $buyer;

        return $this;
    }

    public function isPurchased(): ?bool
    {
        return $this->purchased;
    }

    public function setPurchased(bool $purchased): static
    {
        $this->purchased = $purchased;

        return $this;
    }

    public function isSurprised(): ?bool
    {
        return $this->surprised;
    }

    public function setSurprised(bool $surprised): static
    {
        $this->surprised = $surprised;

        return $this;
    }

    public function getGiftList(): ?GiftList
    {
        return $this->giftList;
    }

    public function setGiftList(?GiftList $giftList): static
    {
        $this->giftList = $giftList;

        return $this;
    }
}
