<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\OpenApi\Model;
use App\Repository\UserRepository;
use App\State\UserPasswordHasher;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(),
        new Post(
            openapi: new Model\Operation(
                security: []
            ),
            security: "is_granted('PUBLIC_ACCESS')",
            validationContext: ['groups' => ['Default', 'user:create']],
            processor: UserPasswordHasher::class
        ),
        new Get(),
        new Patch(securityPostDenormalize: "user == object", processor: UserPasswordHasher::class),
        new Delete(securityPostDenormalize: "user == object"),
    ],
    normalizationContext: ['groups' => ['user:read'], 'enable_max_depth' => true],
    denormalizationContext: ['groups' => ['user:create', 'user:update']],
)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['user:read', 'family:add_user'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['user:create', 'user:update', 'user:read', 'gift:read', 'family:read', 'gift_list:read'])]
    #[MaxDepth(1)]
    private ?string $username = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['user:create', 'user:update', 'user:read', 'family:read'])]
    #[MaxDepth(1)]
    private ?string $picture = null;

    #[Assert\NotBlank]
    #[Assert\Email]
    #[Groups(['user:read', 'user:create', 'user:update'])]
    #[ORM\Column(length: 180, unique: true)]
    #[ApiProperty(security: "user == object")]
    #[MaxDepth(1)]
    private ?string $email = null;

    #[ORM\Column]
    private ?string $password = null;

    #[Assert\NotBlank(groups: ['user:create'])]
    #[Groups(['user:create', 'user:update'])]
    #[MaxDepth(1)]
    private ?string $plainPassword = null;

    #[ORM\Column(type: 'json')]
    private array $roles = [];

    #[ORM\OneToMany(mappedBy: 'buyer', targetEntity: Gift::class)]
    #[Groups(['user:read'])]
    #[MaxDepth(1)]
    private Collection $buyingGifts;

    #[ORM\ManyToMany(targetEntity: Family::class, mappedBy: 'users')]
    #[Groups(['user:read'])]
    #[ApiProperty]
    #[MaxDepth(1)]
    private Collection $families;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: GiftList::class, orphanRemoval: true)]
    #[Groups(['user:read'])]
    #[MaxDepth(1)]
    private Collection $giftLists;

    public function __construct()
    {
        $this->families = new ArrayCollection();
        $this->giftLists = new ArrayCollection();
        $this->buyingGifts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): static
    {
        $this->picture = $picture;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        $this->plainPassword = null;
    }

    /**
     * @return Collection<int, Family>
     */
    public function getFamilies(): Collection
    {
        return $this->families;
    }

    public function addFamily(Family $family): static
    {
        if (!$this->families->contains($family)) {
            $this->families->add($family);
            $family->addUser($this);
        }

        return $this;
    }

    public function removeFamily(Family $family): static
    {
        if ($this->families->removeElement($family)) {
            $family->removeUser($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, GiftList>
     */
    public function getGiftLists(): Collection
    {
        return $this->giftLists;
    }

    /**
     * @param Collection<int, GiftList> $giftLists
     * @return $this
     */
    public function setGiftLists(Collection $giftLists): self
    {
        $this->giftLists = $giftLists;

        return $this;
    }

    public function addGiftList(GiftList $giftList): static
    {
        if (!$this->giftLists->contains($giftList)) {
            $this->giftLists->add($giftList);
            $giftList->setUser($this);
        }

        return $this;
    }

    public function removeGiftList(GiftList $giftList): static
    {
        if ($this->giftLists->removeElement($giftList)) {
            // set the owning side to null (unless already changed)
            if ($giftList->getUser() === $this) {
                $giftList->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Gift>
     */
    public function getBuyingGifts(): Collection
    {
        return $this->buyingGifts;
    }

    public function addBuyingGift(Gift $buyingGift): static
    {
        if (!$this->buyingGifts->contains($buyingGift)) {
            $this->buyingGifts->add($buyingGift);
            $buyingGift->setBuyer($this);
        }

        return $this;
    }

    public function removeBuyingGift(Gift $buyingGift): static
    {
        if ($this->buyingGifts->removeElement($buyingGift)) {
            // set the owning side to null (unless already changed)
            if ($buyingGift->getBuyer() === $this) {
                $buyingGift->setBuyer(null);
            }
        }

        return $this;
    }
}
