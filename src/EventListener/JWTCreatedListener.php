<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class JWTCreatedListener
{

    public function __construct(private TokenStorageInterface $tokenStorage)
    {
    }

    public function onJWTCreated(JWTCreatedEvent $event): void
    {
        $user = $this->tokenStorage->getToken()->getUser();

        $payload       = $event->getData();
        $payload['id'] = $user->getId();

        $event->setData($payload);
    }
}