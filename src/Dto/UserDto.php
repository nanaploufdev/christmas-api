<?php

namespace App\Dto;

use Symfony\Component\Serializer\Annotation\Groups;

class UserDto
{
    #[Groups(['family:add_user'])]
    public string $id;
}