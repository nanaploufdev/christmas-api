<?php

namespace App\Repository;

use App\Entity\Family;
use App\Entity\GiftList;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @extends ServiceEntityRepository<Family>
 *
 * @method Family|null find($id, $lockMode = null, $lockVersion = null)
 * @method Family|null findOneBy(array $criteria, array $orderBy = null)
 * @method Family[]    findAll()
 * @method Family[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FamilyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private readonly TokenStorageInterface $tokenStorage)
    {
        parent::__construct($registry, Family::class);
    }

    public function findFamilyWithoutUsersData(string $token): ?Family
    {
        $user = $this->tokenStorage->getToken()->getUser();
        assert($user instanceof User);
        $queryBuilder = $this->createQueryBuilder('Family')
            ->innerJoin('Family.users', 'FU')
            ->addSelect('FU')
            ->where('Family.token = :token')
            ->setParameter('token', $token);

        $family = $queryBuilder->getQuery()->getOneOrNullResult();

        if ($family) {
            /** @var User $member */
            foreach ($family->getUsers() as $member) {
                $giftLists = $member->getGiftLists()->filter(function (GiftList $giftList) use ($token) {
                    $families = $giftList->getFamilies()->filter(function (Family $family) use ($token) {
                        return $family->getToken() === $token;
                    });
                    return $families->count() > 0;
                });
                $member->setGiftLists($giftLists);
                if ($member->getId() === $user->getId()) {
                    $giftLists = $member->getGiftLists()->filter(function (GiftList $giftList) {
                        return $giftList->isKid();
                    });

                    $member->setGiftLists($giftLists);
                }
            }
        }

        return $family;
    }

//    /**
//     * @return Family[] Returns an array of Family objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('f.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Family
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
