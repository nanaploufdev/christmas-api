<?php

namespace App\Repository;

use App\Entity\GiftList;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @extends ServiceEntityRepository<GiftList>
 *
 * @method GiftList|null find($id, $lockMode = null, $lockVersion = null)
 * @method GiftList|null findOneBy(array $criteria, array $orderBy = null)
 * @method GiftList[]    findAll()
 * @method GiftList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GiftListRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private readonly TokenStorageInterface $tokenStorage)
    {
        parent::__construct($registry, GiftList::class);
    }

    public function findGiftListWithoutUsersData(int $id): ?GiftList
    {
        $user = $this->tokenStorage->getToken()->getUser();
        assert($user instanceof User);
        $queryBuilder = $this->createQueryBuilder('GiftList')
            ->select('GiftList')
            ->where('GiftList.id = :id')
            ->setParameter('id', $id);

        $giftList = $queryBuilder->getQuery()->getOneOrNullResult();
        if ($user === $giftList->getUser()) {
            $queryBuilder
                ->addSelect('GLG')
                ->leftJoin('GiftList.gifts', 'GLG')
                ->andWhere('GLG.surprised = false');
            return $queryBuilder->getQuery()->getOneOrNullResult() ?? $giftList;
        }

        return $giftList;
    }

//    /**
//     * @return GiftList[] Returns an array of GiftList objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('g')
//            ->andWhere('g.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('g.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?GiftList
//    {
//        return $this->createQueryBuilder('g')
//            ->andWhere('g.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
