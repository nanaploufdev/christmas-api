<?php

namespace App\OpenApi;

use ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\OpenApi\Model\MediaType;
use ApiPlatform\OpenApi\Model\Operation;
use ApiPlatform\OpenApi\Model\PathItem;
use ApiPlatform\OpenApi\Model\RequestBody;
use ApiPlatform\OpenApi\OpenApi;
use Symfony\Component\HttpFoundation\Response;

/**
 * Decorates API Platform OpenApiFactory.
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 *
 * @final
 */
class OpenApiFactory implements OpenApiFactoryInterface
{
    public function __construct(
        private readonly OpenApiFactoryInterface $decorated,
        private readonly string $checkPath,
        private readonly string $usernamePath,
        private readonly string $passwordPath
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(array $context = []): OpenApi
    {
        $openApi = $this->decorated->__invoke($context);

        $openApi
            ->getPaths()
            ->addPath($this->checkPath, (new PathItem())->withPost(
                (new Operation())
                ->withSecurity([])
                ->withOperationId('login_check_post')
                ->withTags(['Token Management'])
                ->withResponses([
                    Response::HTTP_OK => [
                        'description' => 'User token created',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    'type' => 'object',
                                    'properties' => [
                                        'token' => [
                                            'readOnly' => true,
                                            'type' => 'string',
                                            'nullable' => false,
                                        ],
                                        'refresh_token' => [
                                            'readOnly' => true,
                                            'type' => 'string',
                                            'nullable' => false,
                                        ],
                                    ],
                                    'required' => ['token', 'refresh_token'],
                                ],
                            ],
                        ],
                    ],
                ])
                ->withSummary('Creates a user token.')
                ->withDescription('Creates a user token.')
                ->withRequestBody(
                    (new RequestBody())
                    ->withDescription('The login data')
                    ->withContent(new \ArrayObject([
                        'application/json' => new MediaType(new \ArrayObject(new \ArrayObject([
                            'type' => 'object',
                            'properties' => $properties = array_merge_recursive($this->getJsonSchemaFromPathParts(explode('.', $this->usernamePath)), $this->getJsonSchemaFromPathParts(explode('.', $this->passwordPath))),
                            'required' => array_keys($properties),
                        ]))),
                    ]))
                    ->withRequired(true)
                )
            ))
        ;

        $openApi
            ->getPaths()
            ->addPath('/token/refresh', (new PathItem())->withPost(
                (new Operation())
                    ->withSecurity([])
                    ->withOperationId('api_refresh_token')
                    ->withTags(['Token Management'])
                    ->withResponses([
                        Response::HTTP_OK => [
                            'description' => 'User token refreshed',
                            'content' => [
                                'application/json' => [
                                    'schema' => [
                                        'type' => 'object',
                                        'properties' => [
                                            'token' => [
                                                'readOnly' => true,
                                                'type' => 'string',
                                                'nullable' => false,
                                            ],
                                            'refresh_token' => [
                                                'readOnly' => true,
                                                'type' => 'string',
                                                'nullable' => false,
                                            ],
                                        ],
                                        'required' => ['token', 'refresh_token'],
                                    ],
                                ],
                            ],
                        ],
                    ])
                    ->withSummary('Refreshes a user token.')
                    ->withDescription('Refreshes a user token.')
                    ->withRequestBody(
                        (new RequestBody())
                            ->withDescription('The refresh token data')
                            ->withContent(new \ArrayObject([
                                'application/json' => new MediaType(new \ArrayObject(new \ArrayObject([
                                    'type' => 'object',
                                    'properties' => $properties = array_merge_recursive($this->getJsonSchemaFromPathParts(['refresh_token'])),
                                    'required' => array_keys($properties),
                                ]))),
                            ]))
                            ->withRequired(true)
                    )
            ))
        ;

        return $openApi;
    }

    private function getJsonSchemaFromPathParts(array $pathParts): array
    {
        $jsonSchema = [];

        if (count($pathParts) === 1) {
            $jsonSchema[array_shift($pathParts)] = [
                'type' => 'string',
                'nullable' => false,
            ];

            return $jsonSchema;
        }

        $pathPart = array_shift($pathParts);
        $properties = $this->getJsonSchemaFromPathParts($pathParts);
        $jsonSchema[$pathPart] = [
            'type' => 'object',
            'properties' => $properties,
            'required' => array_keys($properties),
        ];

        return $jsonSchema;
    }
}
